const { checkLength, checkAlphabet, checkSymbol, checkPassword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit 0 in password to be true', () => {
    expect(checkDigit('0')).toBe(true)
  })
  test('should has digit a in password to be false', () => {
    expect(checkDigit('a')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
  test('should has symbol 11a11 in password to be false', () => {
    expect(checkSymbol('11a11')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should passwrod Seph@2512 to be true', () => {
    expect(checkPassword('Seph@2512')).toBe(true)
  })

  test('should passwrod Seph@25 to be false', () => {
    expect(checkPassword('Seph@25')).toBe(false)
  })
})
