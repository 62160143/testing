const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25
}
const checkAlphabet = function (password) {
  // const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toUpperCase())) return true
  // }
  // return false
  return /[a-z A-Z]/.test(password)
}
const checkDigit = function (password) {
  return /[0-9]/.test(password)
}
const checkSymbol = function (password) {
  const symbols = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbols.includes(ch.toLowerCase())) return true
  }
  return false
}
const checkPassword = function (password) {
  return checkAlphabet(password) && checkLength(password) && checkDigit(password) && checkSymbol(password)
}
module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
